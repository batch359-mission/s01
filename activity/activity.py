name = "Jose"
age = 38
occupation = "writer"
movie = "One more chance"
rating = "99.6"

print(f"Hi my name is {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}")

num1 = 1
num2 = 2
num3 = 3

print(num1 * num2)
print(num1 < num3)
num2 += num3