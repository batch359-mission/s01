# Comments
# Comments in Python are done using the "#" symbol
# ctrl/cmd + / one-line comment

"""
although we have no keybind for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""

# Python Syntax
# Hello world in Python
print("Hello World!")

# Indentation
# Where in other programming languages the indentation in code is for code readability, the indentationin Python is very important.
# In Python, indentation is used to indicate a block of code.

# Variables
# The terminology used for variable names is identifier
# All identifiers should begin with a letter(A to Z), dollar sign($) or an underscore(_)


age = 18
middle_initial ="B"
name1, name2, name3, name4 = "John", "Edwin", "Kenneth", "Rey"
print(name4)

# Data Types
# 1. Strings(str)
full_name = "John Doe"
secret_code = "Pa$$w0rd"

# 2. Numbers (int, float, complex)
num_of_days = 365
pi_approx = 3.1416
complex_num = 1 + 5j

# 3. Boolean (bool)
is_learning = True
is_difficult = False


# Using Variables
print("My name is " + full_name)

# Terminal Outputs
# print() function
print("Hello World Again")
print("My name is " + full_name)
# print("My age is " + age)
print("My age is " + str(age))

# Typecasting
# There may be times when you want to specify a type on to a variable. This can be done with casting. Here are some functions that can be used:
#  1. int() - converts the value into an integer value
# 2. float() - converts the value into a float value
# 3. str() - converts the value into strings

print(int(3.5))
print(int("9876"))
print(float(10))

# F-strings
print(f"Hi! My names is {full_name} and my age is {age}")


# Operations
print(1 + 10) #addition
print(15-8) #subtraction
print(18 * 9) #multiplication
print(21 / 7) #division
print(21 // 7) #floordivision
print(2 ** 6) #exponent
print(18 % 4) #modulo


# Assignment Operators
num1 = 3
num1 += 4 #num1 = num1 + 4
# include -=, *=, /=, %=
print(num1)

# Logical Operators
# AND - and
# OR - or
# NOT -not

print(True and False)
print(not False)
print(False or True)

#comparison operators (returns a boolean value)
print(1 != 1) #false
print(1 == 1) #true
print(5 > 10)
print(5 < 10)
print(1 <=  10)
print(1 >=  10)
